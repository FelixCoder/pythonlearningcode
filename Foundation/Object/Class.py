'''

1.多态
2.类方法类属性 @classmethod
3.静态方法 @staticmethod

注意点：
1.类属性跟类方法是存储的类对象中
2.对象方法跟属性是存储在实例对象中
3.在创建一个对象时，对象中有一个特殊的属性 类对象，它包含了当前类的方法与属性 （这是Python种的一个机制，可以节约空间，不至于创建一个对象就复制一份方法跟属性）


'''

class Train:
    #类属性 用于统计当前类创建对象的次数
    num = 0

    #私有方法
    def __init__(self):
        self.type = '烧煤火车'
        Train.num += 1

    def upSpeed(self):
        print('火车加速中')

    #类方法
    @classmethod
    def addCount(cls):
        print('类方法')

    @classmethod
    def setNum(cls,num):
        cls.num = num

    @classmethod
    def getNum(cls):
        return cls.num

    #静态方法 静态方法中是没有参数的
    @staticmethod
    def delCount():
        print('静态方法')


class HightSpeedTrain(Train):
    def connectWiFi(self):
        print('高铁提供WiFi')

    def upSpeed(self):
        print('加速100KM/H 加速加速')


#多继承 一个类可以同时继承多个类
class HightHightSpeedTrain(HightSpeedTrain,Train):
    def fly(self):
        print('超高速火车 可以有飞机一样的速度')

train = Train()
train.upSpeed() #对象方法调用
Train.addCount() #类方法调用
train.addCount() #类方法调用
Train.delCount() #静态方法调用
train2 = Train()
print(Train.num)

'''
多态体现
'''
train3 = Train()
train4 = HightSpeedTrain()

#多态取决于 你传入什么对象 最终决定调用谁的 方法
def driveCar(car):
    car.upSpeed()

driveCar(train3)
driveCar(train4)