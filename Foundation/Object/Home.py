'''

定义一个房子类
定义一个家具类

计算房屋剩余面积

'''

class Home:
    def __init__(self,area,address,info):
        self.area = area #房屋面积
        self.address = address #房屋地址
        self.info = info #房屋信息
        self.leftArea = area #房屋剩余面积
        self.contains = [] #房屋有哪些家具

    def addFurniture(self,furniture):
        self.contains.append(furniture.getFurnitureName())
        self.leftArea -= furniture.getArea()

    def __str__(self):
        msg = "房子拥有面积为：%d 可用面积为：%d 户型：%s  坐落在：%s "%(self.area,self.leftArea,self.info,self.address)
        msg = msg + "屋内包含家具有：%s"%(str(self.contains))
        return msg

class Furniture:
    def __init__(self,name,area):
        self.area = area
        self.name = name

    def getArea(self):
        return self.area

    def getFurnitureName(self):
        return self.name


'''
往家里添加 家具
'''

home = Home(130,'北京市西城区复兴门大街 333号','3室2厅')

bed = Furniture('床',4)
home.addFurniture(bed)
print(home)

sofa = Furniture('沙发',6)
home.addFurniture(sofa)
print(home)

closet = Furniture('衣柜',10)
home.addFurniture(closet)
print(home)