
'''

烤地瓜
烤地瓜

'''
class SweetPotato:
    def __init__(self):
        self.cookedString = '生的'
        self.cookedLevel = 0 #烤了什么程度了
        self.burdening = [] #烤地瓜配料


    def addBurding(self,item):
        self.burdening.append(item)

    def cooked(self,time):
        self.cookedLevel +=1
        if self.cookedLevel <=0:
            print('生的')
            self.cookedString = '生的'
        elif self.cookedLevel <=3 and self.cookedLevel > 0:
            self.cookedString = '半生半熟'
        elif self.cookedLevel == 5:
            self.cookedString = '熟透了，正好'
        elif self.cookedLevel > 5:
            self.cookedString = '糊了糊了，不好吃'

    def __str__(self):
        return "地瓜熟的程度：%s 配料：%s"%(self.cookedString,str(self.burdening))


'''
创建地瓜对象
'''

sweetPotato = SweetPotato()
sweetPotato.cooked(1)
print(sweetPotato)
sweetPotato.cooked(1)
sweetPotato.addBurding('香蕉')
print(sweetPotato)
sweetPotato.cooked(1)
sweetPotato.addBurding('辣椒')
print(sweetPotato)
sweetPotato.cooked(1)
sweetPotato.addBurding('甜辣酱')
print(sweetPotato)
sweetPotato.cooked(1)
print(sweetPotato)
sweetPotato.cooked(1)
print(sweetPotato)