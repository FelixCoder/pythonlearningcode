'''

1.类的继承
2.方法重写,及父类方法重新调用
注意：
    1.私有属性与方法子类是无法继承跟访问的

'''

class Animal:

    def __init__(self):
        self.name = '动物'
        self.__shape = '圆形' #__shape是属于私有属性

    def eatFood(self):
        print('动物吃东西')

    def drinkWater(self):
        print('动物喝水')

    def sleep(self):
        print('动物睡觉')

    #私有方法 使用双下划线，子类无法访问
    def __eruption(self):
        print('动物喷火')


#Dog继承自Animal 类
class Dog(Animal):
    def dark(self):
        print('小狗汪汪')

    #重写eatFood方法
    def eatFood(self):
        print('小狗要吃肉')

    #重写drinkWater 小狗喝完牛奶还喝水
    def drinkWater(self):
        print('小狗喝牛奶')
        #调用父类方法 方式1
        # Dog.drinkWater(self)
        # 调用父类方法 方式2
        super().drinkWater()

#Cat继承自Animal 类
class Cat(Animal):
    def catch(self):
        print('小猫捉老鼠')


dog = Dog()
dog.dark()
dog.drinkWater()