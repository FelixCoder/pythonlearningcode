'''

1.属性跟方法保护
2.对象引用计数及对象什么时候被销毁

'''

class Dog:
    def __init__(self):
        name = '小花'
        age = 20
    def __str__(self):
        return '%s 现在%d 岁'%(self.getName(),self.getAge())

    def setName(self,name):
        self.name = name
    #可以做额外的处理，避免脏数据
    def setAge(self,age):
        if age > 0:
            self.age = age
        else:
            print('年龄必须大于0')

    def getName(self):
        return self.name

    #可以做额外处理，返回真实数据
    def getAge(self):
        return self.age+10

    #当一个对象的引用计数为0的时候，就会被调用，这个属于Python垃圾回收机制
    def __del__(self):
        print('对象被销毁')


'''

#现在这种写法虽然可以，但容易出现逻辑问题，由于外部修改不可控
dog1 = Dog()
dog1.name = '小明'
dog1.age = -10

#使用方法去控制对象的取值跟设置值，从而保护了数据的真实性
dog2 = Dog()
dog2.setAge(-10)
dog2.setAge(10)
dog2.setName('小花花')
print(dog2)

'''

#对象的引用计数

dog3 = Dog()
dog4 = dog3

#删除一个引用计数 （注意 print打印先后顺序 当把59行 del dog4注释了 '开始'会先输出，再输出 '对象被销毁'"）
del  dog3
 # del  dog4  #当去掉注释 会先输出 '对象被销毁' 再输出  '开始'  从而说明当引用计数为0的情况下，内存会被回收
print('====== 开始 ======')

