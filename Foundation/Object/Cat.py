'''

定义一个类

'''

class Cat:
    def __init__(self,name,age):
        self.name = name;
        self.age = age;

    # def eatFood(): 这种写法会报错 需要填写self
    def eatFood(self):
        print('%s在吃东西'%(self.name))

    def drinkWater(self):
        print('%s在喝水'%(self.name))

    def __str__(self):
        return ("%s现在年龄为：%d\n\n"%(self.name,self.age))

#创建一个对象
cat = Cat('小花',22)
cat.eatFood()
cat.drinkWater()
print(cat)

catt = Cat('小菜',13)
catt.eatFood()
catt.drinkWater()
print(catt)