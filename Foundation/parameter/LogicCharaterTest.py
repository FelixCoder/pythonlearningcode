# or 与 and

height = int(input("请输入身高？"))
weight = int(input("请输入体重"))
if height > 180 and weight < 70:
    print("标准身材")
else:
    print("肥胖")

# not

age = 19
if age > 19 and age < 25:
    print("青年")

if not(age > 19 and age < 25):
    print("非青年")

# elif

if age >= 18:
    print("可以去网吧")
elif age > 25:
    print("可以娶老婆")
elif age > 60:
    print("可以退休了")
else:
    print("刚刚出生")

