#!/usr/bin/env python3
#encoding:utf-8

# 字符串练习
name = 'abcVip'
# title() 将字符串中每个单次首写字母都转换为大写
print(name.title())
# upper() 将字符串中所有字母都转换为大写
print(name.upper())
# lower() 将字符串转换为小写
print(name.lower())
# 将字符串首字母转换成大写
print(name.capitalize())

'''
字符串拼接
'''

# 方式1   使用+
first_name = 'Yin'
last_name = 'Felix'
full_name = first_name + last_name
print(full_name)

# 方式2 使用 %s 拼接方式
full_name2 = "==%s==="%(full_name+last_name)

# lstrip() 去掉字符串开头空格 rstrip()去掉字符串 末尾空格 
strip_str = 'python program   '
print(strip_str)
print(strip_str.rstrip())

print('======================')

lstrip_str = '  python program'
print(lstrip_str.lstrip())
print(lstrip_str)

# 数字转换为 字符串

age = 23
name = 'Felix'
# print(age+name)  会出现错误提示
print(str(age)+name)


# 字符串存储


'''
字符串下标
'''

# 下标是从0开始 ，在取其中的字符时，下标不要超过最大范围 超过为越界

dog = 'FelixTang'

# 顺着取值
print(dog[1])

# 倒着取值
print(dog[-1])

'''
 取一定区间字符(切片) 例如：字符str = "Felix" str[1:3] 表示从索引为1开始取值，3表示结束索引，但不包括3 输出结果为el
 str[1:] 表示从1开始取值到最后一个字符   
 切片[起始位置:结束位置:步长] 步长默认为1，但步长也可以为-1。
 例如[::-1] 表示从右往左取值，从开始位置到结束位置 最终可以输出结果为一个倒序字符串
'''
print(dog[1:3])
# 正序输出
print(dog[::-1])
# 倒序输出
print(dog[::1])

'''
正序，倒序输出结果看，开始与结束索引都没有标明的话，表示从最开始位置到结束位置，至于开始位置从左边开始还是
右边开始 要看步长是为正数还是负数 正数为从左边开始，负数从右边开始
'''


# =============字符串常见操作================

'''
字符串查找
find 查出字符开始索引，从左边开始找 rfind 查出字符开始索引，从右边开始找
count 查找字符串中 字符出现的次数
replace 替换字符串 replace('A','R',1) 将字符串A替换成R 只替换一个，默认情况是替换所有
spli 将字符串切割成一个数组 spli('A') 将字符串按照A字符 进行切割，分成一个数组
startwith,endwith 表示以什么开头，以什么结尾   startwith('A') 表示以A开头 endwith('B') 表示以B结尾
ljust, ljust(50)字符串居左显示 
rjust, rjust(50)字符串居右显示
center center(50)字符串居中显示
'''
find = 'ABCDEABCDE'
print(find.find('BC')) #结果为1 从左边开始找
print(find.rfind('BC')) #结果为6 从右边开始找
print(find.count('BC'))  #结果为2 BC出现过两次
print(find.replace('A','R',1))
print(find.split("BC"))
print(find.startswith("A"))
print(find.endswith("R"))
print(find.ljust(50))
print(find.rjust(50))
print(find.center(50))


# ================== join 使用 ===================

# 将字符串与数组中的字符拼接起来
a = ['A','B','C']
print("".join(a))








