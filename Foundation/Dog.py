# 定义一个类<属性，方法>

'''

在Python中，首字母大写的名称指的是类，这个类定义中的括号是空的。
__init__是一个特殊函数，每当创建实例时Python会自动运行它，在这个方法中，开头和名称有两个下划线，这是一种约定，旨在
避免Python默认方法与普通方法名称发生名称冲突

关于Self:
1.创建实例时，将自动传入实参self.每个与类相关联的方法调用都自动传递实参self,他是一个指向
实例本身的引用，让实例能够访问类中的属性和方法

2.
self.name,self.age定定义的两个变量都有前缀self。以self为前缀的变量都可以供类中的所有方法使用，
我们还可以通过类的任何实例来访问这些变量。




1.类中的函数称为方法

注意：

使用init方法定义函数时，self形参必不可少，还必须位于其他形参的前面



类的编码规范：
类名应采用驼峰命名法（将类名中每个单词的首字母都大写，而不用下划线，实例名和模块名都采用小写格式，并加上下划线）

'''
class Dog():
    def __init__(self,name,age):
        self.name = name
        self.age = age
    def getDogInfo(self):
        print('Dog Name:'+self.name+'\t'+'Dog Age:'+self.age)


'''
# 创建类的实例
'''
first_dog = Dog('Jack','22')

'''
# 调用实例的方法
'''
first_dog.getDogInfo()


'''
# 继承 <扩展属性，方法，重写父类方法>

子类继承父类的所有属性和方法，同时还可以定义自己的属性和方法
'''
class SuperDog(Dog):
    def __init__(self,name,age,nickname):
        super().__init__(name,age)
        self.nickname = nickname
    def getDogInfo2(self):
        print('Dog Name:' + self.name + '\t' + 'Dog Age:' + self.age+'\t'+'Dog NickName:'+self.nickname)

    '''
    重写父类方法
    '''
    def getDogInfo(self):
        print('Dog Name:' + self.name + '\t' + 'Dog Age:' + self.age+'\t'+'Dog NickName:'+self.nickname)
super_dog = SuperDog('SuperDog','122','小黄')
super_dog.getDogInfo()
super_dog.getDogInfo2()


'''
一个模块中存储多个类

例如：
Car
ElectricCar
Battery (电池)
这三个类可做一个模块

'''

'''
一个模块中导入多个类(从模块car中导入Car,ElectriCar类)

from car import Car,ElectriCar
'''


'''
导入整个模块
import car 
'''


'''
导入模块中的所有类 (从模块car中导入所有类)
from car import *
'''

