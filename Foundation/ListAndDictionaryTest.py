# -*- coding=UTF-8 -*-
'''
结合列表与字典实现一个简单的
名片管理系统
1.添加名片
2.查找名片
3.删除名片
4.打印所有名片
5.退出程序
'''

print('1.添加名片')
print('2.查找名片')
print('3.删除名片')
print('4.打印所有名片')
print('5.退出程序')
print('6.保存用户信息')

list = [] #声明一个列表


#程序终止时，保存用户信息成一个文件
def saveUserInfo():
    #先打开一个文件，并申请write写的权限
    data = open('backup.data','w')
    data.write(str(list))
    data.close()

def loadUserInfo():
    global list;
    try:
        #当文件不存在时，程序会奔溃 所添加一个异常捕获解决奔溃问题
        data = open('backup.data','r')
        #注意点 如果要在函数中修改全局变量值时，需要使用global提前声明
        #list没法直接将一个字符串转成list,使用eval可以还原原来保存值的类型
        #data.read 如果在文件特别大的时候，容易导致内存崩溃
        list = eval(data.read())
    except Exception:
        pass


#先加载用户信息
loadUserInfo()
while True:
    index = int(input('请输入程序编号?'))
    if index == 1:
        name = input('请输入姓名：')
        age = input('请输入年龄：')
        address = input('请输入住址：')
        wechat = input('请输入微信号：')
        info = {'name':name,'age':age,'address':address,'wechat':wechat}
        list.append(info)
    elif index == 2:
        name = input('请输入要查找名字：')
        numflag = 0
        for item in list:
            if item['name'] == name:
                print('名字\t年龄\t住址\t微信号')
                print('%s\t%s\t%s\t%s'%(item['name'],item['age'],item['address'],item['wechat']))
                numflag+=1
        else:
            if numflag == 0:
                print('没有找到')
    elif index == 3:
        numflag = 0
        name = input('请输入要删除的名字：')
        for item in list:
            if item['name'] == name:
                numflag += 1
                print('名字\t年龄\t住址\t微信号')
                print('%s\t%s\t%s\t%s'%(item['name'],item['age'],item['address'],item['wechat']))
                list.remove(item)
                print('删除成功！！！')
        else:
            if numflag == 0:
                print('没有找到 要删除的名片')
    elif index == 4:
        for item in list:
            print('名字\t年龄\t住址\t微信号')
            print('%s\t%s\t%s\t%s'%(item['name'],item['age'],item['address'],item['wechat']))
    elif index == 5:
        saveUserInfo()
        break
    elif index == 6:
        saveUserInfo()
    else:
        print('输入有误！！！！')
