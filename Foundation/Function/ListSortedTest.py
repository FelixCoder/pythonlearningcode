'''

对列表进行排序

'''


'''
 列表排序
'''
nums = [23,45,1,90,44,8]
print(nums) #正常输出列表
nums.sort() #正序
nums.reverse() #倒序
print(nums) #输出排序后的列表


'''
对列表中按字典排序
'''
infos = [{'name':'A','age':23,'name':'B','age':23,'name':'C','age':23,}]
infos.sort(key=lambda x:x['name'])


'''
匿名函数


lambda a,b:a+b  是一个整体，作为一个函数 a,b作为参数 a+b表示函数具体实现
'''

# 计算两数字和
def countNum(a,b):
    return a+b

# 多种形式计算 可计算加，减，乘，除
def calculateNum(a,b,func):
    result = func(a,b)
    return result

countNum = calculateNum(12,23,lambda a,b:a+b)
print('两个数字的和是：%d'%(countNum))

countNum = calculateNum(12,23,lambda a,b:a-b)
print('两个数字的差是：%d'%(countNum))

countNum = calculateNum(12,23,lambda a,b:a*b)
print('两个数字的乘积是：%d'%(countNum))

countNum = calculateNum(12,23,lambda a,b:a/b)
print('两个数字的商是：%d'%(countNum))


'''
输入匿名函数  从而说明Python是一个动态语言
Python2 与Python3有些不同
input 输出完匿名函数后，在Python2中可以立即执行，但在Python3
中需要使用eval进行转换
'''

# funtNew = input('请输入要执行的匿名函数:') #这种写法会报错，需要eval转换
funtNew = eval(input('请输入要执行的匿名函数:'))
countNum = funtNew(23,34) #调用匿名函数
print('匿名函数计算结果为:%d'%(countNum))


'''

知识扩充
通过调用testCC与testCCC输出结果得知：num+=num不定价于 num = num+num
num = num+num num会指向num+num新开辟的地址，从而num的结果与实参结果不一样
所以num+=num 是往可变数组中添加元素，从而改变值(前提条件是：实参是一个可变类型的值，比如列表或者字典。如果为数字时可等价)
'''
a = 100

def testDD(number):
    number+=number;
    print("输出结果是:\n%d"%(number))

testDD(a) #输出结果是 100
print(a) #输出结果 100

b = [100]

def testCC(number):
    number+=number;
    print("输出结果是:\n"+str(number))
testCC(b) #结果是 [100,100]
print(b) #结果是 [100,100]

d = [100]
def testCCC(number):
    number=number+number;
    print("输出结果是:\n"+str(number))

testCCC(d) #结果是 [100,100]
print(d) #结果是[100]
