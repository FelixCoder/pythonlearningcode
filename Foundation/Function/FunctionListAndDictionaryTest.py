'''

使用函数实现 简单的名片管理

'''

print('========名片管理系统==========')
print('========1.添加名片==========')
print('========2.查找名片==========')
print('========3.修改名片==========')
print('========4.删除名片==========')
print('========5.打印所有照片==========')
print('========6.退出程序==========')

# 声明列表保存名片数组

cardList = []

# 添加单个名片
def addCard(name,age,address,habit):
    dict = {'name':name,'age':age,'address':address,'habit':habit}
    cardList.append(dict)

# 按照名字查找名片，找到打印出来
def findCard(name):
    num = 0
    for dict in cardList:
        nameStr = dict['name']
        if name == nameStr:
            print('name:%s age:%s address:%s habit:%s'%(dict['name'],dict['age'],dict['address'],dict['habit']))
            num+=1
    else:
        if num == 0:
            print('没有找到')

def modifyCard(name):
    num = 0
    for dict in cardList:
        nameStr = dict['name']
        if name == nameStr:
            print('name:%s age:%s address:%s habit:%s'%(dict['name'],dict['age'],dict['address'],dict['habit']))
            num+=1
            print('1.修改名字')
            print('2.修改年龄')
            print('3.修改地址')
            print('4.修改习惯')
            print('5.修改全部')
            index = int(input('请输入你要修改位置编号:'))
            if index == 1:
                mName = input('请输入要修改的名字：')
                dict['name'] = mName
                num += 1
                print('name:%s age:%s address:%s habit:%s' % (dict['name'], dict['age'], dict['address'], dict['habit']))
                break
            elif index == 2:
                mAge = input('请输入要修改的年龄：')
                dict['age'] = mAge
                num += 1
                print('name:%s age:%s address:%s habit:%s' % (dict['name'], dict['age'], dict['address'], dict['habit']))
                break
            elif index == 3:
                mAddress = input('请输入要修改的地址：')
                dict['name'] = mAddress
                num += 1
                print('name:%s age:%s address:%s habit:%s' % (dict['name'], dict['age'], dict['address'], dict['habit']))
                break
            elif index == 4:
                mHabit = input('请输入要修改的爱好：')
                dict['name'] = mHabit
                num += 1
                print('name:%s age:%s address:%s habit:%s' % (dict['name'], dict['age'], dict['address'], dict['habit']))
                break
            elif index == 5:
                mName = input('请输入要修改的名字：')
                mAge = input('请输入要修改的年龄：')
                mAddress = input('请输入要修改的地址：')
                mHabit = input('请输入要修改的爱好：')
                dict['name'] = mName
                dict['age'] = mAge
                dict['address'] = mAddress
                dict['habit'] = mHabit
                num += 1
                print('name:%s age:%s address:%s habit:%s' % (dict['name'], dict['age'], dict['address'], dict['habit']))
            else:
                print('输入有误')
    else:
        if num == 0:
            print('没有找到需要修改的名片')


def deleteCard(name):
    num = 0
    for dict in cardList:
        nameStr = dict['name']
        if name == nameStr:
            cardList.remove(dict)
            print('名片删除成功！')
            num+=1
    else:
        if num == 0:
            print('没找到需要删除的名片')


def printAllCard():
    for dict in cardList:
        print('name:%s age:%s address:%s habit:%s' % (dict['name'], dict['age'], dict['address'], dict['habit']))

while True:
    num = int(input('请输入要操作的序号：'))
    if num == 1:
        name = input('请输入姓名:')
        age = input('请输入年龄:')
        address = input('请输入地址:')
        habit = input('请输入爱好:')
        addCard(name,age,address,habit)
    elif num == 2:
        name = input('请输入要查找的姓名')
        findCard(name)
    elif num == 3:
        name = input('请输入要修改的姓名')
        modifyCard(name)
    elif num == 4:
        name = input('请输入要删除的姓名')
        deleteCard(name)
    elif num == 5:
        printAllCard()
    elif num == 6:
        break
    else:
        print('输入有误！！！！')