
# 导入整个模块 Module

import Module

'''
函数的优之一是，使用它们可将代码 与主程序分 。
通过函数定名 ，可让主程序容易理解得多。
你还可以更进一步，将函数存储在被为的独立文件中，再将模块导入到主程序中。
import语句允许在当前运行的程序文件中使用模块中的代码。
通过将函数存储在独立的文件中，可隐藏程序代码的细节，将重点放在程序的高层逻辑上。 
这还能让你在众多不同的程序中重用函数。
将函数存储在独立文件中后，可与其他程序员共享这些文件而不是整个程序。
知道如何 导入函数还能让你使用其他程序员编写的函数 。
导入的方法有多种，下面对每种都作简要的介绍。


1.导入整个模块
2.导入特定的函数
3.使用as 给函数指定别名
4.使用as 给模块指定别名
5.导入模块中的所有函数


注意：
Python中函数命名特点及规范。应给函数指定描述性名称，且只在其中使用小写字母和下划线。
描述性名称可帮助你和别人明白代码想要做什么。模块命名也应遵循上述约定

'''


'''
调用模块Module中函数
moduleFunction
moduleFunction1
'''
Module.moduleFunction()
Module.moduleFunction1()


'''
导入特定函数 (moduleFunction)
from Module import moduleFunction
'''

'''
使用as 给函数指定别名 (函数名为 fun)
from Module import moduleFunction as fun
'''

'''
使用as 给模块指定别名 (命名为 Mo)
import Module as Mo
'''


'''
导入模块中的所有函数(使用星号*运算符可让Python导入模块中的所有函数)

from Module import *
'''

