
# 导入模块
import Module

'''
# 导入模块中特定函数 from module_name import function_name
'''
from Module import moduleFunction1

'''
# 使用as给模块中的函数指定别名
'''
from Module import moduleFunction2 as Two
print('函数开始')


'''
# 定义一个简单结构的函数
'''
def userInfo1():
    print('Hello World')
'''
# 函数调用
'''
userInfo1()


'''
# 定义一个 参数有默认值的函数
'''
def userInfo2(name='参数默认值'):
    print(name)
userInfo2()
userInfo2('Tom')



'''
# 带参数的函数
'''
def userInfo3(name):
    print(name)

'''
# 函数调用(传递参数方式有 '位置传参'，'关键词传参')
'''
userInfo3('Felix')
userInfo3(name='Jack')

'''
# 带任意数量的实参
#形参名*users中的星
#让Python创建一个名为users空元组，并将到的有值都装到这个元组中。
#函数体内的print语句通过生成输出来证明Python能够处理使用一个值调用函数的情形，也能处理使用三个值来调用函数的情形。
#它以类似的方式处理不同的调用，注意， Python将实参装到一个元组中，即便函数只收到一个值也如此
'''

def userInfo4(*users):
    print(users)

'''
   结合使用位置石材和任意数量实参
   如果要让函数接受不同类型实参，必须在函数定义中将接纳任意数量实参的形参放在最后
'''
def userInfo7(name,*users):
    print(name+str(users))


'''
# 使用任意数量的关键字实参 (键值对形式)
# 形参**users中的两个星让Python创建一个名为users的空字典，并将收到的所有名称-值对都封装到这个字典中。
# 在这个函数中，可以像访问其他字典那样访问user_info中的名称-值对
'''

def userInfo5(**users):
    print(users)

'''
# 定义输出用户名的函数
'''
def get_formatted_name(f_name, l_name):
    full_name = f_name + ' ' + l_name
    return full_name

while True:
    print('\n告诉我你的名字')
    fname = input("First Name\t")
    lname = input("Last Name\t")
    formatedname = get_formatted_name(fname, lname)
    print('你的全名是：' + formatedname)
    result = input("是否需要退出？Yes(退出) No（继续）")
    if result == 'Yes':
        break



'''
# 调用模块中的函数
'''
Module.moduleFunction()

'''
# 调用模块中特定函数
'''
moduleFunction1()

'''
# 调用别名的函数
'''
Two()


'''
  全局变量与局部变量
  
  注意点：当申明的全局变量在函数中进行修改时，仅仅直接赋值是不能进行修改的 需要在函数中使用 global做一个说明,才可进行修改
  但没有使用global进行申明时，只能认为函数中有一个与全局变量有相同变量名的局部变量
'''
parameter1 = 33

def wendu():
    # parameter1 = 66
    # global  parameter1 使用global 关键字做一个申明 parameter1是一个全局变量，当没有申明时 表示是一个局部变量
    parameter1 = 66
    print('parameter %d'%parameter1)
    return parameter1

def huashiwendu():
    result = wendu() + 3
    print('华氏温度%d'%result)

wendu()
print('global parameter %d'%parameter1)
huashiwendu()


'''
  Python内存问题：
  a = 10
  b = a
  表示a 指向 10的内存地址
  b也之前a指向的地址，Python与C语言内存管理不一样、
  
  c语言是 a内存中存储 10,b内存中存储a中的值，相当于两块内存空间存储同一个值

   id(变量名) 可以得到变量指向的地址
   
   所以Python中 = 左边的变量都是'引用'右边值的地址
'''
a = 100
b = a
print(id(a)) #a引用 100的内存地址 b引用a指向的地址
print(id(b))

A = ['11','22']
B = A
print(id(A))
print(id(B))

'''
Python中 哪些类型是可以变化的，哪些是不可变化的 数字，字符串，元组三种都是不可变的，其他都可变 列表与字典

'''
str = 'Hello'
str[0] = 'B' # 修改H的值为B,但会报错 所以说明字符串的值是不可以修改的
print(str)
str = 'JJJ' #虽然str的值改变了，只不过是str指向新的内存地址 从而指向了新的内容

mapmap = {3.14:'jjjjj',(33,44):'alkdfj','jjjj':'aljljl'}
print(mapmap) #这个正常输出 数字，字符串，元组可以作为key 由于他们是不可变类型，在hash时 值是不变的

mapmap = {3.14:'jjjjj',(33,44):'alkdfj','jjjj':'aljljl',[22,33]:'报错'} # 这样会报错，由于列表是可变的，所以不能作为key
print(mapmap)


'''

递归

阶乘 5！= 5 * 4 * 3 * 2 * 1    4! = 4*3*2*1

5的阶乘等于 5 乘以4的阶乘

递归一定注意结束条件，要不然 会出现内存暴增 直到程序死亡

'''

#函数中调用函数自己 表示递归
def getNums(num):
    if num > 1:
       return num * getNums(num - 1)
    else:
        return num

getNums(5)











