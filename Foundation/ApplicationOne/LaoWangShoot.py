'''
老王大枪
1.老王
2.枪
3.子弹
4.弹夹
5.敌人
'''

#人
class Person(object):
    def __init__(self,name):
        #人名
        self.name = name;
        #手枪
        self.gun = None;
        #血量
        self.hp = 100;

    def __str__(self):
        # str = self.name+"还有"+self.gun+"剩余血量"+self.hp
        return '敌人死了，%s还有%s发子弹，剩余血量%i'%(self.name,self.gun,self.hp);

    #拿枪
    def holdGun(self,gun):
        self.gun = gun;

    #打枪
    def shoot(self,enemy):
        #拿出一发子弹
        print('%s向%s打枪' % (self.name,enemy.name))
        bullet = self.gun.clip.bullets[0]
        #敌人中弹
        result = enemy.getShot(bullet)
        # 移除弹夹中的第一发子弹
        self.gun.delete()
        return result;

    #中弹
    def getShot(self,bullet):
        self.hp -= bullet.power;
        if self.hp <= 0:
            print('%s 挂了'%(self.name));
            return False
        else:
            print('还有机会,记得躲子弹');
            return True

    #添加弹夹
    def addClip(self,clip):
        self.gun.addClip(clip)

#手枪
class Gun(object):
    def __init__(self,clip):
        self.clip = clip

    def __str__(self):
        return str(self.clip)

    #添加弹夹
    def addClip(self,clip):
        self.clip = clip;

    #往弹夹里添加子弹
    def put(self,bullet):
        self.clip.add(bullet);

    #移除弹夹里子弹
    def delete(self):
        self.clip.remove();

#子弹夹
class Clip(object):
    def __init__(self,maxNum):
        #弹夹最多能装多少子弹
        self.maxNum = maxNum
        #使用数组保存子弹
        self.bullets = [];

    def __str__(self):
        return str(len(self.bullets))

    #保存子弹
    def add(self,bullet):
        count = 10#self.bullets.count;
        if count < self.maxNum:
           self.bullets.append(bullet);
        else:
            print('子弹装满了')

    #移除第一发子弹
    def remove(self):
        self.bullets.remove(self.bullets[0])
#子弹
class Bullet(object):
    def __init__(self,power):
        #子弹
        self.power = power

    def __str__(self):
        return '威力'+str(self.power)


def main():

    #创建老王
    laowang = Person('老王')

    #创建弹夹
    bulletNum = 20
    clip = Clip(bulletNum);

    #弹夹中装子弹
    for i in range(bulletNum):
        #创建子弹
        bullet = Bullet(10)
        clip.add(bullet)

    #创建手枪
    gun = Gun(None);
    #拿起手枪
    laowang.holdGun(gun)
    #往手枪中装弹夹
    laowang.addClip(clip)
    #创建敌人
    enemy = Person('敌人')

    #老王打敌人
    for i in range(bulletNum):
        result = laowang.shoot(enemy)
        if result:
            continue;
        else:
            print('%s'%(laowang))
            break;


#调用程序
main()


