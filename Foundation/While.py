# 接收用户输入 while
num = int(input('请输入数字'))

while num > 9:
    print(str(num))
    num = num - 5

#打印三角形  print 拼接 end =""就不会换行

num = 1
while num <= 10:
    i = num;
    while i>0:
        # 不换行
        print("*",end="")
        i-=1;
    #     换行
    print("")
    num+=1;
#     打印矩形

print("\n\n\n\n\n\n")
num = 1;
while num <= 10:
    i = 1;
    while i <= 10:
        print("*",end="")
        i+=1
    print("")
    num+=1



# 乘法表

num = 1
while num <= 9:
    i = 1
    while i <= num:
        print("%d*%d = %d "%(i,num,i*num),end="")
        i+=1;
    print("")
    num+=1;
