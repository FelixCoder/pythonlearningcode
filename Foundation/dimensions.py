#encoding:utf-8
'''
# 元组(Python 将不能修改的值称为不可变的,而不可变的列表被称为元组)
# 元组看起来犹如列表，但使用圆括号而不是方括号标识。定义元组后，就可以使用索引来访问其元素，
就像访问列表元素一样
#元组不支持修改，增加，删除 

'''

print('元组开始:')

# 遍历元组 与遍历 列表是一样的，由于元组中元素值是不能被改变的，如果尝试修改的话，运行会报错
demensions = (200,50)
print(demensions)
print(demensions[0]) #去除元组中的值
for demension in demensions:
	print(demension)
print('Hello')


# 注意点

print("\n\n元组注意点：")

a = (11,22)
b = a
print(b) #正常赋值

c,d = a #这个是将a元组中拆分开 分别赋值,但拆分的value个数一定要与变量个数相等 如果不相等会报错

print(c)
print(d)