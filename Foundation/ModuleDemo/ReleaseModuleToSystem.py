
'''

知识点1 将自己开发的模块安装到系统中（可以方便在整个系统中使用）

#第一步😝 在当前路径下创建一个setup.py文件，写入以下内容 （固定格式）
'''
# 导入TestMes包下sendmsg模块
'''
from distutils.core import setup
setup(name='Felix',version='1.0',description='模块测试',author='FelixYin',py_modules=['TestMes.sendmsg'])

#第二部😝 执行构建模块命令
python3 setup.py build

#第三步😝 执行生成发布压缩包命令
python3 setup.py sdist


1.创建模块
2.发布模块
3.安装使用模块

'''