# 可以控制别人使用方法与非公开方法 []内的是公开方法
__all__ = ['test1','test2']

def test1():
    print('test1')

def test2():
    print('test2')
