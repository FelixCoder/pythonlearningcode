'''

列表生成式
python2中：
range(10)
range(10,18)
range(10,18,2)

python3中：
list = [i for i in range(1,18)]
'''
# lista = range(10)
# listb = range(10,18)
# listc = range(10,18,2)
# print(str(lista))
# print(str(listb))
# print(str(listc))


#列表生成式 1
#输出结果：[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
list1 = [i for i in range(2,18)]
print(list)


#列表生成式 2 将range生成的数组遍历出的每一个值，使用临时变量i保存，同时i值每次都会保存到数组中
#输出结果：[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
list2 = [i for i in range(2,18)]
print(list2)


#列表生成式3 多层嵌套，但最终还是只保存i到数组中，后面控制i值重复次数
#输出结果：[0, 0, 1, 1, 2, 2]
list3 = [i for i in range(3) for j in range(2)]
print(list3)

#列表生成式4 多层嵌套，但最终还是只保存i到数组中，后面控制i值重复次数
#输出结果：[(0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)]
list4 = [(i,j) for i in range(3) for j in range(2)]
print(list4)

#列表生成式5 多层嵌套，但最终还是只保存i到数组中，后面控制i值重复次数
#输出结果：[(0, 0, 0), (0, 0, 1), (0, 0, 2), (0, 1, 0), (0, 1, 1), (0, 1, 2), (1, 0, 0), (1, 0, 1), (1, 0, 2), (1, 1, 0), (1, 1, 1), (1, 1, 2), (2, 0, 0), (2, 0, 1), (2, 0, 2), (2, 1, 0), (2, 1, 1), (2, 1, 2)]
list5 = [(i,j,k) for i in range(3) for j in range(2) for k in range(3)]
print(list5)

#多层嵌套 可以表示如下方式：
list = []
for i in range(3):
    for j in range(2):
        for k in range(3):
            list.append((i,j,k))
print('=============')
print(list)



'''
set 集合 没有重复值
list 列表 有序的
'''
print('===============')
#生成一个包含有3字符串的集合
sett = set('3')
#向集合中添加一个新元素
sett.add('4')
print(sett) #输出结果：{'4', '3'}
sett.add('4')
print(sett) #输出结果：{'4', '3'} ,表明集合中只能保存一个值，不会保存第二个相同的值