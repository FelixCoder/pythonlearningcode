'''
知识点2 给程序传参

1.导入系统模块sys
2.使用模块sys中的参数argv接受给程序传输过来的参数

python3 transfer.py 参数

'''
#导入系统模块
import sys

name = sys.argv
print(name)