'''
定义一个汽车店类
'''
class Carstore(object):
   def __init__(self):
     #初始化时 创建一个工厂类来生产工厂
     self.factory = Factory()

   def saleCar(self,cartype):
      return self.factory.selectedCarType(cartype);


'''
简单工厂模式
'''
class Factory(object):
  def selectedCarType(cartype):
    if cartype == '桑塔纳':
       return Santana();
    elif cartype == '奔驰':
       return Benzi();


'''
定义一个汽车基类
'''
class Car(object):

    pass

    def move(self):
        #汽车可移动
        pass
    def music(self):
        #汽车可播放音乐
        pass

'''
定义一个汽车1
'''
class Benzi(Car):
    def wifi(self):
        print('可以上网')

'''
定义一个汽车2
'''
class Santana(Car):
    pass

'''
在汽车店 购买一辆汽车
'''

store = Carstore();
# 购买汽车
store.saleCar('桑塔纳')